﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SmartCardSA;

namespace SCFrameWorkTester
{
    public partial class frmMain : Form
    {
        private string[] readers;
        private SmartCardSAClass sc;

        public frmMain()
        {
            InitializeComponent();
            FormClosing += new FormClosingEventHandler(frmMain_FormClosing);
            sc = new SmartCardSAClass();
            readers = sc.getReaders();
            loadReaders();
        }

        void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            sc.disconnect();
        }

        private void loadReaders()
        {
            foreach (string reader in readers)
            {
                cmbReaders.Items.Add(reader);
            }
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            byte[] bytes = sc.readName();
            tbxName.Text = Utils.convertByteArrayToString(bytes);
            bytes = sc.readSurname();
            tbxSurname.Text = Utils.convertByteArrayToString(bytes);
            bytes = sc.readID();
            tbxID.Text = Utils.byteArrayToHexString(bytes).Replace("F", "");
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (tbxPin.Text.Equals(""))
            {
                MessageBox.Show("Please enter a pin");
                return;
            }
            if (tbxName.Text.Equals(""))
            {
                MessageBox.Show("Please enter the name");
                return;
            }
            try
            {
                sc.updateName(tbxName.Text, tbxPin.Text);
                sc.updateSurname(tbxSurname.Text, tbxPin.Text);
                sc.updateID(tbxID.Text, tbxPin.Text);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Pin not valid"))
                {
                    MessageBox.Show("Pin not valid");
                    tbxPin.Text = "";
                }
                else
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void cmbReaders_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbReaders.SelectedIndex > -1)
            {
                btnConnect.Enabled = true;
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                sc.connect(cmbReaders.SelectedIndex);
                btnRead.Enabled = true;
                btnUpdate.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
