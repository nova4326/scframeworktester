﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCardFramework;

namespace SmartCardSA
{
    public class SmartCardSAClass
    {
        private APDUResponse response;
        private APDUParam apduParam;
        private CardNative iCard;
        private string[] readers;
        private static APDUCommand
            apduVerifyPIN = new APDUCommand(0x00, 0x20, 0, 1, null, 0),
            apduSelectFile = new APDUCommand(0x00, 0xA4, 0, 0, null, 0),
            apduWriteFile = new APDUCommand(0x00, 0xD0, 0, 2, null, 0),
            apduUpdateFile = new APDUCommand(0x00, 0xD6, 0, 0, null, 0),
            apduReadRecord = new APDUCommand(0x00, 0xB0, 0, 0, null, 16);

        private const ushort SC_OK = 0x9000;
        private const byte SC_PENDING = 0x9F;
        private bool cardPresent;

        public SmartCardSAClass()
        {
            try
            {
                iCard = new CardNative();                
                readers = iCard.ListReaders();
                iCard.StartCardEvents(readers[0]);
                iCard.OnCardInserted += new CardInsertedEventHandler(iCard_OnCardInserted);
                iCard.OnCardRemoved += new CardRemovedEventHandler(iCard_OnCardRemoved);
            }
            catch (Exception ex)
            {
                throw new Exception("SmartCard: " + ex.Message);
            }
        }

        void iCard_OnCardRemoved(string reader)
        {
            cardPresent = false;
        }

        void iCard_OnCardInserted(string reader)
        {
            cardPresent = true;
        }

        public void connect(int readerIndex)
        {
            if (cardPresent)
            {
                iCard.Connect(readers[readerIndex], SHARE.Shared, PROTOCOL.T1);
            }
            else
            {
                throw new Exception("No card present");
            }
        }

        public bool verifyPin(string pin)
        {
            try
            {
                byte[] bytes = Utils.hexStringToByteArray(pin);
                apduParam = new APDUParam();
                apduParam.Data = bytes;
                apduVerifyPIN.Update(apduParam);
                response = iCard.Transmit(apduVerifyPIN);
                if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                    throw new Exception("Pin Failed: " + response.ToString());
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public byte[] readName()
        {
            try
            {
                List<byte> byteList = new List<byte>();
                apduParam = new APDUParam();
                apduParam.Data = new byte[] { 0x00, 0x01 };
                apduSelectFile.Update(apduParam);
                response = iCard.Transmit(apduSelectFile);
                if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                    throw new Exception("Select command failed: " + response.ToString());
                apduParam.Data = null;
                apduReadRecord.Update(apduParam);
                response = iCard.Transmit(apduReadRecord);
                if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                    throw new Exception("Binary Read failed: " + response.ToString());
                foreach (byte b in response.Data)
                {
                    if (b != 255)
                    {
                        byteList.Add(b);
                    }
                }
                byte[] bytes = byteList.ToArray();
                return bytes;
            }
            catch (Exception ex)
            {
                throw new Exception("readName: " + ex.Message, ex);
            }
        }

        public bool updateName(string name, string pin)
        {
            try
            {
                apduParam = new APDUParam();
                apduParam.Data = new byte[] { 0x00, 0x01 };
                apduSelectFile.Update(apduParam);
                response = iCard.Transmit(apduSelectFile);
                if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                    throw new Exception("Select command failed: " + response.ToString());
                if (verifyPin(pin))
                {
                    byte[] bytes = Utils.convertStringToByteArray(name);
                    bytes = Utils.fillArray(bytes, 16);
                    apduParam.Data = bytes;
                    apduUpdateFile.Update(apduParam);
                    response = iCard.Transmit(apduUpdateFile);
                    if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                        throw new Exception("Data Update failed: " + response.ToString());
                    return true;
                }
                else
                {
                    throw new Exception("Pin not valid");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("updateName: " + ex.Message, ex);
            }
        }

        public byte[] readSurname()
        {
            try
            {
                List<byte> byteList = new List<byte>();
                apduParam = new APDUParam();
                apduParam.Data = new byte[] { 0x00, 0x02 };
                apduSelectFile.Update(apduParam);
                response = iCard.Transmit(apduSelectFile);
                if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                    throw new Exception("Select command failed: " + response.ToString());
                apduParam.Data = null;
                apduReadRecord.Update(apduParam);
                response = iCard.Transmit(apduReadRecord);
                if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                    throw new Exception("Binary Read failed: " + response.ToString());
                foreach (byte b in response.Data)
                {
                    if (b != 255)
                    {
                        byteList.Add(b);
                    }
                }
                byte[] bytes = byteList.ToArray();
                return bytes;
            }
            catch (Exception ex)
            {
                throw new Exception("readSurname: " + ex.Message, ex);
            }
        }

        public bool updateSurname(string surname, string pin)
        {
            try
            {
                apduParam = new APDUParam();
                apduParam.Data = new byte[] { 0x00, 0x02 };
                apduSelectFile.Update(apduParam);
                response = iCard.Transmit(apduSelectFile);
                if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                    throw new Exception("Select command failed: " + response.ToString());
                if (verifyPin(pin))
                {
                    byte[] bytes = Utils.convertStringToByteArray(surname);
                    bytes = Utils.fillArray(bytes, 16);
                    apduParam.Data = bytes;
                    apduUpdateFile.Update(apduParam);
                    response = iCard.Transmit(apduUpdateFile);
                    if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                        throw new Exception("Data Update failed: " + response.ToString());
                    return true;
                }
                else
                {
                    throw new Exception("Pin not valid");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("updateSurname: " + ex.Message, ex);
            }
        }

        public byte[] readID()
        {
            try
            {
                List<byte> byteList = new List<byte>();
                apduParam = new APDUParam();
                apduParam.Data = new byte[] { 0x00, 0x04 };
                apduSelectFile.Update(apduParam);
                response = iCard.Transmit(apduSelectFile);
                if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                    throw new Exception("Select command failed: " + response.ToString());
                apduParam.Data = null;
                apduReadRecord.Update(apduParam);
                response = iCard.Transmit(apduReadRecord);
                if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                    throw new Exception("Binary Read failed: " + response.ToString());
                foreach (byte b in response.Data)
                {
                    if (b != 255)
                    {
                        byteList.Add(b);
                    }
                }
                byte[] bytes = byteList.ToArray();
                return bytes;
            }
            catch (Exception ex)
            {
                throw new Exception("readID: " + ex.Message, ex);
            }
        } 

        public bool updateID(string id, string pin)
        {
            try
            {
                apduParam = new APDUParam();
                apduParam.Data = new byte[] { 0x00, 0x04 };
                apduSelectFile.Update(apduParam);
                response = iCard.Transmit(apduSelectFile);
                if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                    throw new Exception("Select command failed: " + response.ToString());
                if (verifyPin(pin))
                {
                    byte[] bytes = Utils.hexStringToByteArray(Utils.padString(id, 14));
                    bytes = Utils.fillArray(bytes, 7);
                    apduParam.Data = bytes;
                    apduUpdateFile.Update(apduParam);
                    response = iCard.Transmit(apduUpdateFile);
                    if (response.Status != SC_OK && response.SW1 != SC_PENDING)
                        throw new Exception("Data Update failed: " + response.ToString());
                    return true;
                }
                else
                {
                    throw new Exception("Pin not valid");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("updateID: " + ex.Message, ex);
            }
        }

        public string[] getReaders()
        {
            return readers;
        }

        public void disconnect()
        {
            iCard.StopCardEvents();
            iCard.Disconnect(DISCONNECT.Unpower);
        }
    }
}
