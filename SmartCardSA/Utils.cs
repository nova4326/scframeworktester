﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCardSA
{
    public class Utils
    {
        public static byte[] hexStringToByteArray(string hex)
        {
            if (hex.Length % 2 == 1)
                throw new Exception("The binary key cannot have an odd number of digits");

            byte[] arr = new byte[hex.Length >> 1];

            for (int i = 0; i < hex.Length >> 1; ++i)
            {
                arr[i] = (byte)((getHexVal(hex[i << 1]) << 4) + (getHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        public static int getHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            return val - (val < 58 ? 48 : 55);
            //For lowercase a-f letters:
            //return val - (val < 58 ? 48 : 87);
            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }

        public static string byteArrayToHexString(byte[] bytes)
        {
            string hex = BitConverter.ToString(bytes).Replace("-", string.Empty);
            return hex;
        }

        public static string convertByteArrayToString(byte[] bytes)
        {
            string result = System.Text.Encoding.UTF8.GetString(bytes);

            return result;
        }

        public static byte[] convertStringToByteArray(string str)
        {
            byte[] bytes = new byte[str.Length];
            int count = 0;
            foreach (char c in str)
            {
                bytes[count] = Convert.ToByte(c);
                count++;
            }
            return bytes;
        }

        public static byte[] fillArray(byte[] bytes, int length)
        {
            if (length >= bytes.Length)
            {
                byte[] newBytes = new byte[length];
                for (int i = 0; i < bytes.Length; i++)
                {
                    newBytes[i] = bytes[i];
                }
                return newBytes;
            }
            else
            {
                throw new Exception("Length specified is smaller than the length of the given array");
            }
        }

        public static string padString(string str, int length)
        {
            if (length >= str.Length)
            {
                string newStr = str;
                for (int i = str.Length; i < length; i++)
                {
                    newStr = newStr + "F";
                }
                return newStr;
            }
            else
            {
                throw new Exception("Length specified is smaller than the length of the given string");
            }
        }
    }
}
